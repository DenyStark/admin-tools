# Admin tools

v0.5.1

Admin tools for all admin panels.

[![NPM](https://img.shields.io/npm/v/ttm-react-library.svg)](https://www.npmjs.com/package/ttm-react-library) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```
yarn add @denystark/ttm-admin-tools
```

## Usage

1. Install library.
2. Import React components.
3. Import css styles from `node_modules/${library_name}/dist/index.css` in **app.js**.
4. Add props.

## License

## Storybook building

1. Папка example
   nvm use 12
   yarn install

2. Корневая папка:
   nvm use 12
   yarn install
   yarn build

3. Папка example
   yarn build-storybook

Публикуем данные из storybook-static

MIT © [ToTheMoon](https://gitlab.com/tothemoongame1)
