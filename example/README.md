This example was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

It is linked to the ttm-react-library package in the parent directory for development purposes.

You can run `yarn install` and then `yarn start` to test your package.

```
1. Папка example
nvm use 12
yarn install

2. Корневая папка:
nvm use 12
yarn install
yarn build

3. Папка example
yarn build-storybook

storybook-static



script:
    - ssh app@172.104.154.134 'cd DIR && git checkout . && git pull'
    - ssh app@172.104.154.134 'cd DIR/example && yarn'
    - ssh app@172.104.154.134 'cd DIR && yarn && yarn build'
    - ssh app@172.104.154.134 'cd DIR/example && yarn build-storybook'


```
