import React from 'react'
import {
  FormItem,
  PaginationBlock,
  DropdownMultiply,
  useDropdownMultiply
} from 'ttm-react-library'
import 'ttm-react-library/dist/index.css'
const initElement = { id: 'all', title: 'All' }
const list = [
  { id: 'field1', title: 'Field 1' },
  { id: 'field2', title: 'Field 2' },
  { id: 'field3', title: 'Field 3' },
  { id: 'field4', title: 'Field 4' },
  { id: 'field5', title: 'Field 5' }
]

const pages = 15

const App = () => {
  const [currentPage, setCurrentPage] = React.useState(5)
  const { currentItem, itemList, changeItem, id, title } = useDropdownMultiply(
    list,
    'id',
    'title'
  )
  const setPage = React.useCallback(
    (page) => () => {
      setCurrentPage(page)
    },
    []
  )
  console.log(currentItem, itemList, changeItem, id, title)

  return (
    <div
      style={{
        height: '500px',
        width: '100%',
        // display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        background: 'white',
        position: 'relative',

        padding: '12px'
      }}
    >
      {' '}
      <DropdownMultiply
        current={currentItem}
        itemList={itemList}
        changeItem={changeItem}
        id={id}
        title={title}
        name='Module'
        customClass='page_orders-module'
        //   transform={transformDD}
      />
      <PaginationBlock totalPages={5} currentPage={2} />
    </div>
  )
}

export default App
