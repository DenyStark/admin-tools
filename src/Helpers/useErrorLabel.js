import React from 'react'

const isEqualFields = (errorField, label) => {
  return errorField.toLowerCase() === label.toLowerCase().split(' ').join('')
}

export const useErrorLabel = (error, label) => {
  const [innerError, setInnerError] = React.useState(false)
  React.useEffect(() => {
    if (!error) {
      setInnerError(false)
      return
    }
    if (typeof error === 'boolean') {
      setInnerError(true)
    } else {
      setInnerError(isEqualFields(error, label))
    }
  }, [error, label])

  return innerError
}
