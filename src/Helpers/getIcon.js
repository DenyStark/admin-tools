import { ReactComponent as Dashboard } from '../theme/assets/images/icons/leftMenuIcons/dashboard.svg'
import { ReactComponent as Analytics } from '../theme/assets/images/icons/leftMenuIcons/analytics.svg'
import { ReactComponent as Chat } from '../theme/assets/images/icons/leftMenuIcons/chat.svg'
import { ReactComponent as ExternalPurchases } from '../theme/assets/images/icons/leftMenuIcons/external-purchases.svg'
import { ReactComponent as Items } from '../theme/assets/images/icons/leftMenuIcons/items.svg'
import { ReactComponent as KeyValues } from '../theme/assets/images/icons/leftMenuIcons/key-values.svg'
import { ReactComponent as Languages } from '../theme/assets/images/icons/leftMenuIcons/languages.svg'
import { ReactComponent as Logs } from '../theme/assets/images/icons/leftMenuIcons/logs.svg'
import { ReactComponent as Models } from '../theme/assets/images/icons/leftMenuIcons/models.svg'
import { ReactComponent as Orders } from '../theme/assets/images/icons/leftMenuIcons/orders.svg'
import { ReactComponent as Stats } from '../theme/assets/images/icons/leftMenuIcons/stats.svg'
import { ReactComponent as Predictions } from '../theme/assets/images/icons/leftMenuIcons/predictions.svg'
import { ReactComponent as Support } from '../theme/assets/images/icons/leftMenuIcons/support.svg'
import { ReactComponent as Users } from '../theme/assets/images/icons/leftMenuIcons/users.svg'
import { ReactComponent as Tournaments } from '../theme/assets/images/icons/leftMenuIcons/tournaments.svg'
import { ReactComponent as Monitor } from '../theme/assets/images/icons/leftMenuIcons/monitor.svg'
import { ReactComponent as Promo } from '../theme/assets/images/icons/leftMenuIcons/promo.svg'
import { ReactComponent as Blacklist } from '../theme/assets/images/icons/leftMenuIcons/blacklist.svg'
import { ReactComponent as Index } from '../theme/assets/images/icons/leftMenuIcons/index.svg'
import { ReactComponent as Payments } from '../theme/assets/images/icons/leftMenuIcons/payments.svg'
import { ReactComponent as Products } from '../theme/assets/images/icons/leftMenuIcons/products.svg'
import { ReactComponent as Withdrawals } from '../theme/assets/images/icons/leftMenuIcons/withdrawals.svg'
import { ReactComponent as Deposits } from '../theme/assets/images/icons/leftMenuIcons/deposits.svg'
import { ReactComponent as RefRewards } from '../theme/assets/images/icons/leftMenuIcons/refRewards.svg'
import { ReactComponent as Invest } from '../theme/assets/images/icons/leftMenuIcons/invest.svg'
import { ReactComponent as Quest } from '../theme/assets/images/icons/leftMenuIcons/quest.svg'
import { ReactComponent as AtmOrders } from '../theme/assets/images/icons/leftMenuIcons/atmorders.svg'
import { ReactComponent as RubOrders } from '../theme/assets/images/icons/leftMenuIcons/ruborders.svg'
import { ReactComponent as PromoCodes } from '../theme/assets/images/icons/leftMenuIcons/promocodes.svg'
import { ReactComponent as Result } from '../theme/assets/images/icons/leftMenuIcons/result.svg'
import { ReactComponent as Mail } from '../theme/assets/images/icons/leftMenuIcons/mail.svg'
import { ReactComponent as Autofarm } from '../theme/assets/images/icons/leftMenuIcons/autofarm.svg'
import { ReactComponent as Pools } from '../theme/assets/images/icons/leftMenuIcons/pools.svg'
import { ReactComponent as Ticket } from '../theme/assets/images/icons/leftMenuIcons/ticket.svg'
import { ReactComponent as Swaps } from '../theme/assets/images/icons/leftMenuIcons/swaps.svg'
import { ReactComponent as Key } from '../theme/assets/images/icons/leftMenuIcons/key.svg'
import { ReactComponent as Crown } from '../theme/assets/images/icons/leftMenuIcons/crown.svg'
import { ReactComponent as Notices } from '../theme/assets/images/icons/leftMenuIcons/notices.svg'

export default {
  dashboard: Dashboard,
  key: Key,
  crown: Crown,
  analytics: Analytics,
  chat: Chat,
  externalPurchases: ExternalPurchases,
  items: Items,
  keyValues: KeyValues,
  languages: Languages,
  logs: Logs,
  models: Models,
  orders: Orders,
  predictions: Predictions,
  stats: Stats,
  support: Support,
  users: Users,
  tournaments: Tournaments,
  monitor: Monitor,
  blacklist: Blacklist,
  promo: Promo,
  index: Index,
  payments: Payments,
  products: Products,
  withdrawals: Withdrawals,
  deposits: Deposits,
  refRewards: RefRewards,
  promoCodes: PromoCodes,
  rubOrders: RubOrders,
  atmOrders: AtmOrders,
  quest: Quest,
  invest: Invest,
  result: Result,
  mail: Mail,
  autofarm: Autofarm,
  pools: Pools,
  ticket: Ticket,
  swaps: Swaps,
  notices: Notices
}
