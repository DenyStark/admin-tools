import {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react'

export function useEvent(fn) {
  const fnRef = useRef(fn)

  useLayoutEffect(() => {
    fnRef.current = fn
  }, [fn])

  const eventCb = useCallback(
    (...args) => {
      return fnRef.current.apply(null, args)
    },
    [fnRef]
  )

  return eventCb
}

export function isFunction(value) {
  return typeof value === 'function'
}

const defaultDeserialize = (v) => v

export function useSearchParamsState({
  name,
  serialize = (v) => v,
  deserialize = defaultDeserialize
}) {
  const [value, setValue] = useState(() => {
    const initialValue = deserialize(getSearchParam(location.search, name))

    return initialValue
  })

  useEffect(() => {
    const currentValue = deserialize(getSearchParam(location.search, name))

    if (String(currentValue) !== String(value)) {
      setValue(currentValue)
    }
  }, [location.search])

  const updateValue = useEvent((newValue) => {
    const search = window.location.search

    const actualNewValue = isFunction(newValue) ? newValue(value) : newValue

    setValue(actualNewValue)

    const newSearch = setSearchParam(search, name, serialize(actualNewValue))
    history.pushState(null, '', `${location?.pathname}?${newSearch}`)
  })

  return [value, updateValue]
}

function getSearchParam(search, param) {
  const searchParams = new URLSearchParams(search)
  return searchParams.get(param)
}

function setSearchParam(search, param, value) {
  const searchParams = new URLSearchParams(search)
  searchParams.set(param, value)
  return searchParams.toString()
}
