export const getCurrentData = (data, type, activeTypes) => {
  return data?.filter((item) => {
    return activeTypes?.findIndex((elem) => elem === item[type].toString()) > -1
  })
}
export const modifiedData = (data, short) => {
  if (short) {
    return `${data.substr(0, 10)}`
  }
  return `${data.substr(0, 10)} ${data.substr(11, 5)}`
}

export const replaceStr = (str, length) => {
  if (str.length < length) {
    return str
  }
  return `${str.slice(0, length)}...`
}
export const firstCharUpper = (str) => {
  return str[0].toUpperCase() + str.slice(1)
}
export const run = (value, ...func) => {
  return func.reduce((prev, current) => {
    return current(prev)
  }, value)
}

export const splitStringBySeparator = (str, separator = /(?=[A-Z])/) =>
  str
    .split(separator)
    .reduce((acc, curr) => {
      return `${acc} ${curr}`
    }, '')
    .trim()

export const shortWallet = wallet => `${wallet.slice(0, 6)}...${wallet.slice(-4)}`;

export const getPaymentInfo = (walletLink, transactionLink) => (id, type) => {
  let url;
  switch (type) {
    case 'wallet':
      url = walletLink;
      break;
    case 'transaction':
      url = transactionLink;
      break;
    default:
      return null;
  }
  return `${url}${id}`;
};
