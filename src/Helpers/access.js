// const accessOld = (accesses, pages) =>
//   accesses.findIndex((item) => item === pages) < 0

export const access = (accesses, checkAccess) => {
  if (!accesses) {
    return true
  }
  return !(accesses.filter((item) => item.indexOf(checkAccess) > -1).length > 0)
}

// const accesses = [
//   'investClub+',
//   'users++',
//   'orders+',
//   'questResults',
//   'logs',
//   'products+'
// ]

// const pages = 'users+++'

// const res = access(accesses, pages)
// console.log(res)
