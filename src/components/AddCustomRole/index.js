import React from 'react'
import './style.scss'
import { ClickOutComponent } from '../../Helpers'

const InputField = React.memo(({ item, changeList }) => {
  const ref = React.useRef(item)
  const [val, setVal] = React.useState(item || '')
  const refInput = React.useRef()
  const handleEvent = (cb) => (e) => {
    cb(e.target.value)
  }
  console.log(ref)

  const blur = () => {
    changeList((it) => {
      const findIdx = it.findIndex((el) => el === ref.current)
      if (!val) {
        return [...it.slice(0, findIdx), ...it.slice(findIdx + 1)]
      }
      return [...it.slice(0, findIdx), val, ...it.slice(findIdx + 1)]
    })
  }

  const onKey = React.useCallback(() => {
    refInput.current.style.width = `${
      (refInput.current.value.length + 1) * 5 + 15
    }px`
  }, [])
  React.useEffect(() => {
    onKey()
  }, [onKey])
  return (
    <input
      className='custom-role-add-list access-list-element active gfs-tiny'
      value={val}
      onBlur={blur}
      onKeyDown={onKey}
      ref={refInput}
      onChange={handleEvent(setVal)}
    />
  )
})

const AddCustomRole = React.memo(({ initialRole, changeRoleList }) => {
  const [roleList, setRoleList] = React.useState(initialRole)
  const [newField, setNewField] = React.useState('Add custom role')
  const ref = React.useRef(null)
  // const onKey = React.useCallback(() => {
  //   ref.current.style.width = `${(ref.current.value.length + 1) * 5 + 15}px`;
  // }, []);
  const handleEvent = (cb) => (e) => {
    cb(e.target.value)
  }
  const clearField = () => {
    setNewField('')
  }
  React.useEffect(() => {
    changeRoleList(roleList)
  }, [changeRoleList, roleList])
  const blur = React.useCallback(
    (e) => {
      if (!newField) {
        setNewField(() => 'Add custom role')
        return
      }
      if (newField && newField !== 'Add custom role') {
        setRoleList((val) =>
          [...val, newField].filter(
            (el, idx, arr) => arr.findIndex((v) => v === el) === idx
          )
        )
        setNewField(() => 'Add custom role')
      }

      if (e.target.className.includes('div-add-input')) {
        ref.current.focus()
      }
    },
    [newField]
  )
  // React.useEffect(() => {
  //   onKey();
  // }, [newField, onKey]);
  return (
    <div className='custom-role'>
      <div className='custom-role-add'>
        {roleList.map((item) => (
          <InputField item={item} key={item} changeList={setRoleList} />
        ))}{' '}
        <ClickOutComponent onClickOut={blur}>
          <input
            className='add-input access-list-element  gfs-tiny'
            value={newField}
            onChange={handleEvent(setNewField)}
            //  onBlur={blur}
            onFocus={clearField}
            ref={ref}
            //   onKeyDown={onKey}
          />
        </ClickOutComponent>
        {newField !== 'Add custom role' && newField !== '' ? (
          <div
            type='button'
            className='add-input access-list-element div-add-input gfs-tiny'
          >
            Add custom role
          </div>
        ) : null}
      </div>
    </div>
  )
})

export default AddCustomRole
