import React from 'react'
import ButtonWithTooltip from '../Tooltip/ButtonWithTooltip'
import '../../theme/assets/styles/global.scss'
import '../FormButton/style.scss'
import './style.scss'

const PageHeader = React.memo(({ title, comment, cb, btnText, t }) => {
  return (
    <div className='page-header'>
      <div className='page-header-title gfs-h1'>{t(title)}</div>
      <div className='page-header-comment gfs-body'>{t(comment)}</div>
      {cb && (
        <ButtonWithTooltip>
          <button
            className='form-button white page-header-button'
            type='button'
            onClick={cb}
          >
            {btnText}
          </button>
        </ButtonWithTooltip>
      )}
    </div>
  )
})
export default PageHeader
