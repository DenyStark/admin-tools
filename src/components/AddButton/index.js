import React from 'react'
import '../../theme/assets/styles/global.scss'

const AddButton = React.memo(({ click }) => {
  return (
    <button type='button' onClick={click} className='add-button'>
      +
    </button>
  )
})
export default AddButton
