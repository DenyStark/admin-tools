import React from 'react'
import './style.scss'

const CurrentValue = React.memo(
  ({ title = 'Current value', value, type = 'number', currency }) => {
    const [defaultValue, setDefaultValue] = React.useState('')
    React.useEffect(() => {
      switch (type) {
        case 'number':
          setDefaultValue('0.00')
          break
        case 'count':
          setDefaultValue('0')
          break
        case 'string':
          setDefaultValue('')
          break
        default:
          return null
      }
    }, [type])

    return (
      <div className='current_value'>
        {title}:
        <span className='gfs-small-bold'>
          {typeof value === 'boolean'
            ? value.toString()
            : value || defaultValue}
          {currency}
        </span>
      </div>
    )
  }
)
export default CurrentValue
