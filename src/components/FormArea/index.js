import React, { useCallback, useState, useRef, useEffect } from 'react'
import './style.scss'
import { access, useErrorLabel } from '../../Helpers'

const FormArea = React.memo(
  ({
    label,
    onChange,
    value,
    additionalClass = '',
    noAutocomplete,
    small,
    color,
    checkAccess,
    accesses,
    autoHeight,
    heightFitContent,
    error,
    ...props
  }) => {
    const [customClass, setCustomClass] = React.useState(additionalClass)
    const [type] = useState(props.type || 'text')
    const input = useRef(null)
    const innerError = useErrorLabel(error, label)

    // const { status, checker } = useChangeReadOnly();
    const highlight = () => {
      setCustomClass((prev) => `${prev} error`)
    }

    useEffect(() => {
      if (!autoHeight) return
      const area = input.current

      if (area) {
        if (heightFitContent) {
          area.style.height = 'auto'
        }

        const scrollHeight = area.scrollHeight

        const valueHeight =
          scrollHeight && scrollHeight > 38 && scrollHeight < 80
            ? scrollHeight
            : scrollHeight > 80
            ? 80
            : 40

        area.style.height = heightFitContent
          ? scrollHeight + 'px'
          : valueHeight + 'px'
      }
    }, [input, value, autoHeight, heightFitContent])

    const handleChange = useCallback(
      (e) => {
        setCustomClass((prev) => prev.replace('error', ''))
        onChange(e)
      },
      [onChange]
    )
    return (
      <div className={`form-area-group ${color} ${innerError ? 'error' : ''}`}>
        <textarea
          ref={input}
          {...props}
          name={`real-${props.name}`}
          type={type}
          className={`form-area gfs-body ${customClass} ${
            value?.toString().length > 0 ? 'not-empty' : ''
          } ${small ? 'small' : ''}`}
          value={props.type === 'password' ? undefined : value}
          onChange={handleChange}
          onInvalid={highlight}
          readOnly={
            checkAccess ? access(accesses, checkAccess) : props.readOnly
          }
          placeholder={label}
        />
        {noAutocomplete && (
          <input
            className='autocomplete_hack'
            name={`faketest-${props.name}`}
            type={type}
          />
        )}
        {label && <div className='form-area__label  gfs-small'>{label}</div>}
      </div>
    )
  }
)

export default FormArea
