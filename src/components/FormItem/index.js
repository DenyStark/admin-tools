import React, { useCallback, useState, useRef } from 'react'
// import { useChangeReadOnly } from 'Helpers';
import './style.scss'
import '../../theme/assets/styles/global.scss'
import { access, useErrorLabel } from '../../Helpers'
// import { ReactComponent as Eye } from 'Theme/assets/images/icons/ic_eye.svg';

const FormItem = React.memo(
  ({
    label,
    onChange,
    value,
    additionalClass = '',
    noAutocomplete,
    small,
    color,
    checkAccess,
    accesses,
    error,
    link,
    linkParams,
    children,
    linkValue = (i) => i,
    ...props
  }) => {
    const [customClass, setCustomClass] = React.useState(additionalClass)
    const [type] = useState(props.type || 'text')
    const innerError = useErrorLabel(error, label)
    const input = useRef(null)

    const highlight = () => {
      setCustomClass((prev) => `${prev} error`)
    }

    const handleChange = useCallback(
      (e) => {
        setCustomClass((prev) => prev.replace('error', ''))
        onChange(e)
      },
      [onChange]
    )

    const labelClass = ['form-item__label', 'gfs-small']

    if (!value && type !== 'date') {
      labelClass.push('form-item__hide')
    }

    return (
      <div className={`form-item-group ${color}  ${innerError ? 'error' : ''}`}>
        {' '}
        {link && value ? (
          <a
            className={`form-item gfs-body ${customClass} ${
              value?.toString().length > 0 ? 'not-empty' : ''
            } ${small ? 'small' : ''}`}
            href={value}
            {...linkParams}
          >
            {linkValue(value)}
          </a>
        ) : (
          children || (
            <input
              ref={input}
              {...props}
              name={`real-${props.name}`}
              type={type}
              className={`form-item gfs-body ${customClass} ${
                value?.toString().length > 0 ? 'not-empty' : ''
              } ${small ? 'small' : ''}`}
              value={props.type === 'password' ? undefined : value}
              onChange={handleChange}
              readOnly={
                checkAccess ? access(accesses, checkAccess) : props.readOnly
              }
              onInvalid={highlight}
              placeholder={
                props.required && !label.includes('*') ? label + ' *' : label
              }
            />
          )
        )}
        {noAutocomplete && (
          <input
            className='autocomplete_hack'
            name={`faketest-${props.name}`}
            type={type}
          />
        )}
        {label && (
          <div className={labelClass.join(' ')}>
            {props.required && !label.includes('*') ? label + ' *' : label}
          </div>
        )}
      </div>
    )
  }
)

export default FormItem
