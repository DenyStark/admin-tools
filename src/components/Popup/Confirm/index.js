import React from 'react'
import './style.scss'
import FormButton from '../../FormButton'
import '../../../theme/assets/styles/global.scss'
import Popup from '../index'

const ConfirmPopup = React.memo(
  ({ hide, confirm, access, accesses, filtredFunction }) => {
    return (
      <Popup
        customClass='confirm-popup'
        hide={hide}
        filtredFunction={filtredFunction}
      >
        <div className='confirm-popup-title gfs-h2'>Confirm your action</div>
        <div className='confirm-popup-button'>
          <FormButton
            color='black'
            type='button'
            onClick={confirm}
            checkAccess={access}
            accesses={accesses}
          >
            Confirm
          </FormButton>
          <FormButton color='black' type='button' onClick={hide}>
            Cancel
          </FormButton>
        </div>
      </Popup>
    )
  }
)
export default ConfirmPopup
