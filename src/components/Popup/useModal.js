import React from 'react'

const useModal = () => {
  const [open, setOpen] = React.useState(false)
  const hide = () => {
    setOpen(false)
  }
  const show = () => {
    setOpen(true)
  }

  React.useEffect(() => {
    if (open) {
      document.body.style.overflow = 'hidden'
    }
    return () => {
      document.body.style = null
    }
  }, [open])

  return {
    open,
    show,
    hide
  }
}
export default useModal
