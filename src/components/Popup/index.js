import React from 'react'
import ReactDOM from 'react-dom'
import useModal from './useModal'
import { ReactComponent as Close } from '../../theme/assets/images/icons/close.svg'
import './style.scss'
import '../../theme/assets/styles/global.scss'
import { ClickOutComponent } from '../../Helpers'

const Popup = React.memo(
  ({
    hide,
    customClass = '',
    children,
    filtredFunction,
    noClose,
    parentId = 'root'
  }) => {
    const parent = document.getElementById(parentId)
    const ref = React.useRef()

    const handleClickOut = React.useCallback(
      (e) => {
        e.stopPropagation()
        if (filtredFunction && filtredFunction(e)) {
          return
        }
        hide()
      },
      [hide]
    )

    return ReactDOM.createPortal(
      <div className='popup-overlay'>
        <ClickOutComponent
          onClickOut={handleClickOut}
          eventTypes={['mousedown']}
        >
          <div ref={ref} className={`popup-wrapper ${customClass}`}>
            {!noClose ? (
              <button className='close-button' type='button' onClick={hide}>
                <Close />
              </button>
            ) : null}
            {children}
          </div>
        </ClickOutComponent>
      </div>,
      parent
    )
  }
)

export default Popup
export { useModal }
