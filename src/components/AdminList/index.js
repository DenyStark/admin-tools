import React from 'react'
import { ReactComponent as Icon } from '../../theme/assets/images/icons/dropdownIcon-big.svg'
import Dropdown, { useDropdown } from '../Dropdown'
import './style.scss'

// const loc = window.location.toString()
const transform = (_, item) => {
  return (
    <div className='adminList-items'>
      <div className='gfs-h2'>{item.title}</div>
      <div className='gfs-small'>{item.description}</div>
      <Icon />
    </div>
  )
}

const findCurrentLink = (field) => (location, item) => {
  return location.includes(item[field])
}

const linkWithField = findCurrentLink('link')

export const AdminListDropdown = React.memo(({ list, loc }) => {
  const currentEl = list.find((item) => linkWithField(loc, item))
  const { currentItem, itemList, changeItem, id, title } = useDropdown(
    list,
    'key',
    'title',
    currentEl
  )
  const cb = (el) => {
    window.open(el.link, '_self')
  }

  return (
    <div className='adminList-dropdown'>
      {' '}
      <Dropdown
        current={currentItem}
        itemList={itemList}
        changeItem={changeItem}
        id={id}
        title={title}
        name='Status'
        customClass='adminList-dropdown-inner'
        transform={transform}
        cb={cb}
      />
    </div>
  )
})

const AdminList = React.memo(({ list, loc }) => {
  return (
    <React.Fragment>
      {list && list.length ? (
        <div className='adminList'>
          {list?.map((item) => (
            <a
              className={`adminList-link ${
                linkWithField(loc, item) ? 'active' : ''
              }`}
              key={item.id}
              href={item.link}
            >
              <div className={`adminList-link-element ${item.key}`}>
                <div className='title gfs-h2'>{item.title}</div>
                <div className='description gfs-small'>{item.description}</div>
              </div>
            </a>
          ))}
        </div>
      ) : null}
    </React.Fragment>
  )
})
export default AdminList
