import React from 'react'

const getErrorField = (str) => {
  if (!str) {
    return ''
  }
  const getFieldList = str.match(/(^'|')(\S{1,})('|'$)/)
  if (!getFieldList) {
    return ''
  }
  return getFieldList.find((el) => !el.includes("'"))
}

const useError = () => {
  const [error, setError] = React.useState('')
  const [errorField, setErrorFeld] = React.useState('')

  const addError = (...val) => {
    const valuesToString = val.join(' ')
    setError(valuesToString)
    setErrorFeld(getErrorField(valuesToString))
  }
  const removeError = () => {
    setError('')
    setErrorFeld('')
  }
  return {
    error,
    errorField,
    addError,
    removeError
  }
}
export default useError
