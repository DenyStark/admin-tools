import React from 'react'
import { useInterval } from '../../Helpers'
import useError from './useError'
import './style.scss'

const ErrorMessage = React.memo(({ message, close, timeout = 100000 }) => {
  useInterval(close, timeout)

  return <div className='error-message'>{message}</div>
})
export default ErrorMessage
export { useError }
