import Popup, { useModal } from '../Popup'
import React from 'react'
import SearchPanelButton from '../ToolButton'
import './style.scss'

const FilterBlock = React.memo(
  ({ children, maxItems = 3, customClass, customAction, wrapperStyle }) => {
    const { open, show, hide } = customAction || useModal()

    if (Array.isArray(children) && children.length >= maxItems) {
      return (
        <div className='filter-block'>
          <SearchPanelButton
            type='button'
            icon='fas fa-filter'
            click={show}
            active={open}
          />
          {open ? (
            <Popup
              hide={hide}
              customClass={`filter-block-popup ${customClass}`}
              noClose
            >
              <div className='filter-block-children' style={wrapperStyle}>
                {children}
              </div>
            </Popup>
          ) : null}
        </div>
      )
    }
    return <React.Fragment>{children}</React.Fragment>
  }
)
export default FilterBlock
