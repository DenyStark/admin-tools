import React, { useEffect, useState } from 'react'
import FormButton from '../FormButton'
import FormItem from '../FormItem'
import CSVDownloads from '../CsvDownload'
import './style.scss'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const ms = 86400000
// const idx = 13;
const setNewArray = ({ from, to }) => {
  const initialValue = new Date(to).getTime()
  const dateCount = initialValue - new Date(from).getTime()
  return {
    initialValue,
    daysCount: (dateCount + ms) / ms
  }
}

const DateRange = React.memo(
  ({
    dateRange,
    setRange,
    getValues,
    csvDownload,
    csvName = 'all-stats',
    labelFrom,
    labelTo,
    automatic,
    buttonName = 'Apply',
    disabled,
    withQueryParam
  }) => {
    const [to, setTo] = React.useState(dateRange.to)
    const [from, setFrom] = React.useState(dateRange.from)

    const [toQuery, setToQuery] = useSearchParamsState({
      name: 'to',
      deserialize: (v) => v || ''
    })

    const [fromQuery, setFromQuery] = useSearchParamsState({
      name: 'from',
      deserialize: (v) => v || ''
    })

    useEffect(() => {
      if (dateRange) {
        setTo(dateRange.to)
        setFrom(dateRange.from)
      }
    }, [dateRange])

    const [init, setInit] = useState(false)

    const changeDataRange = React.useCallback(
      (initChange, initClick) => {
        if (initChange) {
          if (fromQuery || toQuery) {
            setRange({
              ...setNewArray({ to: toQuery, from: fromQuery }),
              to: toQuery,
              from: fromQuery
            })
            if (getValues) {
              getValues()
            }
          }
        } else {
          setRange({ ...setNewArray({ to, from }), to, from })
          if (getValues) {
            getValues()
          }
        }

        const initStep =
          withQueryParam && (!fromQuery || !toQuery) && initChange
        const changeStep = withQueryParam && initChange && initClick

        if (initStep || changeStep) {
          setToQuery(to)
          setFromQuery(from)
        }

        if (!init) {
          setInit(true)
        }
      },
      [getValues, to, from, setRange, withQueryParam, toQuery, fromQuery]
    )

    useEffect(() => {
      if (!withQueryParam && !init) return

      if (init && (fromQuery || toQuery)) {
        changeDataRange(init)
      }
    }, [init, toQuery, fromQuery])

    React.useEffect(() => {
      if (automatic) {
        return
      }

      changeDataRange(init)
    }, [])

    React.useEffect(() => {
      if (!automatic || !setRange) {
        return
      }
    }, [automatic, changeDataRange, from, to, setRange])

    const handleChange = (cb) => (name) => (event) => {
      cb(event.target.value)

      if (setRange && automatic) {
        setRange({
          ...setNewArray({ to, from, [name]: event.target.value }),
          to,
          from,
          [name]: event.target.value
        })
      }
    }

    return (
      <div className='range'>
        <FormItem
          label={labelFrom ? `${labelFrom} from` : 'From'}
          color='white'
          value={from}
          type='date'
          onChange={handleChange(setFrom)('from')}
        />
        <FormItem
          label={labelTo ? `${labelTo} to` : 'To'}
          color='white'
          value={to}
          type='date'
          onChange={handleChange(setTo)('to')}
        />{' '}
        {automatic ? null : (
          <FormButton
            color='black'
            type='button'
            onClick={() => changeDataRange(true, true)}
            customClass='close'
            disabled={disabled}
          >
            {buttonName}
          </FormButton>
        )}
        {csvDownload && (
          <CSVDownloads
            cb={csvDownload}
            filename={csvName}
            icon='fas fa-download'
          />
        )}
      </div>
    )
  }
)
export default DateRange
