import FormItem from '../FormItem'
import FormArea from '../FormArea'
// import FormButton from '../FormButton'
import React from 'react'
import CurrentValue from '../CurrentValue'
import './style.scss'

export const useUpdate = (initialObject = {}) => {
  const data = React.useRef({ ...initialObject })
  const setData = React.useCallback((value, field) => {
    data.current[field] = value
  }, [])
  return {
    data: data.current,
    setData
  }
}

const UpdateForm = React.memo(
  ({
    callback = () => {},
    initValue,
    label,
    field = 'field',
    access,
    currentValue,
    accesses,
    error,
    formType,
    ...props
  }) => {
    const [data, setData] = React.useState(initValue || '')
    const handleChange = (cb) => (event) => cb(event.target.value)
    const mountRef = React.useRef(false)

    React.useEffect(() => {
      if (mountRef.current) {
        callback(data, field)
      }
      mountRef.current = true
    }, [callback, data, field])

    return (
      <div className={`updateForm ${field}`}>
        {formType === 'area' ? (
          <FormArea
            label={label || field}
            color='white'
            value={data}
            onChange={handleChange(setData)}
            checkAccess={access}
            accesses={accesses}
            error={error}
            {...props.item}
          />
        ) : (
          <FormItem
            label={label || field}
            color='white'
            value={data}
            onChange={handleChange(setData)}
            checkAccess={access}
            accesses={accesses}
            error={error}
            {...props.item}
          />
        )}
        {currentValue ? (
          <CurrentValue value={props.current?.cb ? props.current.cb(initValue) : initValue} {...props.current} />
        ) : null}
      </div>
    )
  }
)

export default UpdateForm
