import React from 'react'
import { toast, cssTransition } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import './style.scss'

toast.configure()
const notify = (toastMessage, time, type) =>
  toast[type](toastMessage, {
    position: 'top-right',
    autoClose: time,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    transition: swirl
  })
const swirl = cssTransition({
  enter: 'swirl-in-fwd',
  exit: 'swirl-out-bck'
})

const Notices = React.memo(({ list, remove, time = 3000 }) => {
  React.useEffect(() => {
    if (list.length > 0) {
      if (list[0].type === 'error') {
        notify(
          <CustomNotice
            title={list[0].title}
            message={list[0].message}
            error
          />,
          time,
          'error'
        )
      } else {
        notify(
          <CustomNotice title={list[0].title} message={list[0].message} />,
          time,
          'success'
        )
      }
      remove()
    }
  }, [list, remove])

  return null
})

const CustomNotice = React.memo(({ title, message, error }) => {
  return (
    <div className={`custom-notice ${error ? 'error' : ''}`}>
      <div className='title'>{title}</div>
      <div className='message'>{message}</div>
    </div>
  )
})

export default Notices
