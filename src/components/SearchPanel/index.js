import React, { Fragment, useEffect, useRef } from 'react'

import FormItem from '../FormItem'
import FormButton from '../FormButton'
import { useInterval } from '../../Helpers'
import ErrorMessage, { useError } from '../ErrorMessage'
import './style.scss'
import ButtonWithTooltip from '../Tooltip/ButtonWithTooltip'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const SearchPanel = React.memo(
  ({
    label,
    customClass,
    children,
    left,
    right,
    refresh,
    func,
    searchParams,
    changeSearchParams,
    withQueryParam
  }) => {
    const { error, addError, removeError } = useError()
    const [data, setData] = React.useState('')
    const [anim, setAnim] = React.useState(false)
    // const handleChange = (cb) => (event) => cb(event.target.value)
    const [value, setValue] = useSearchParamsState({
      name: 'pattern',
      deserialize: (v) => v || ''
    })

    const [page, setPage] = useSearchParamsState({
      name: 'page',
      deserialize: (v) => v || ''
    })

    const useInit = useRef(null)

    useEffect(() => {
      if (!withQueryParam && !useInit.current) return

      if (data || (data === '' && value)) {
        setValue(data)
      }
    }, [data, useInit])

    useEffect(() => {
      if (!withQueryParam || useInit.current) return

      if (value) {
        setData(value)
        ;(async () => {
          try {
            await changeSearchParams(value.trim())
            await func()
            if (Number(page) !== 1) {
              setPage(1)
            }
          } catch (err) {
            addError(err.response?.data.message, err.response?.data.data)
          }
        })()
      }
      useInit.current = true
    }, [value, useInit])

    const submit = async (e) => {
      e.preventDefault()
      if (data === searchParams) {
        return
      }
      setAnim(false)
      try {
        await changeSearchParams(data.trim())
        await func()
      } catch (err) {
        addError(err.response?.data.message, err.response?.data.data)
      }
    }
    const reset = async () => {
      setAnim(true)
      refresh()

      //  setAnim(false);
    }
    useInterval(() => setAnim(false), 2000)

    React.useEffect(() => {
      setData(searchParams)
      return () => {}
    }, [searchParams])

    return (
      <Fragment>
        <div className={`search-panel ${customClass}`}>
          {left && (
            <div className='search-panel-content'>
              <form className='search-panel-form' onSubmit={submit}>
                <FormItem
                  additionalClass='search-panel-form-item'
                  color='white'
                  label={label}
                  value={data}
                  onChange={(e) => setData(e.target.value)}
                />
                <FormButton type='submit' color='black'>
                  <i className='fas fa-search' />
                </FormButton>
              </form>
            </div>
          )}
          {right && (
            <div className='search-panel-children'>
              {children}
              <ButtonWithTooltip>
                <FormButton type='button' color='white' onClick={reset}>
                  <i className={`fas fa-sync-alt ${anim ? 'anim' : ''}`} />
                </FormButton>
              </ButtonWithTooltip>
            </div>
          )}
        </div>{' '}
        {error ? <ErrorMessage message={error} close={removeError} /> : null}
      </Fragment>
    )
  }
)
export default SearchPanel
