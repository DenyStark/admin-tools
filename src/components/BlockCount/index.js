import React from 'react'
import './style.scss'

const BlockCount = React.memo(({ title, text, small }) => {
  return (
    <div className='block-count'>
      <div className='gfs-h1 title'>{title ?? '--'}</div>
      <div className={`${small ? 'gfs-small' : 'gfs-body'} text`}>{text}</div>
    </div>
  )
})

export default BlockCount
