import React from 'react'
import { shortWallet, getPaymentInfo } from '../../Helpers/transform'
import './style.scss'

const SelectedLink = React.memo(({ link, field, walletLink, transactionLink }) => {
  const inputRef = React.useRef(null);

  const open = () => {
    window.open(getPaymentInfo(walletLink, transactionLink)(link, field), '_blank', 'noopener noreferrer');
  };
  const copy = () => {
    navigator.clipboard.writeText(link);
  };

  return (
    <div className='selection-input'>
      <div style={{ height: '1px', width: '1px', overflow: 'hidden' }} ref={inputRef}>
        {link}
      </div>

      <div className='selection-input-text'>{shortWallet(link)}</div>
      <div className='selection-input-buttons'>
        <button className='selection-input-button' type='button' onClick={open}>
          <i className='fas fa-external-link-alt' />
        </button>
        <button className='selection-input-button' type='button' onClick={copy}>
          <i className='fas fa-copy' />
        </button>
      </div>
    </div>
  );
});
export default SelectedLink
