import React from 'react'
import './style.scss'

const FlowElement = React.memo(({ name, description, percent, currency }) => {
  return (
    <div className='flow-element'>
      <div className='flow-element-title'>
        {name}
        {currency ? (
          <div className='flow-element-currency'>{currency}</div>
        ) : null}
      </div>
      <div className='flow-element-bar'>
        <div
          className='flow-element-bar-outer'
          style={{ transform: `translateX(${percent}%)` }}
        />
      </div>
      <div className='flow-element-data'>{description}</div>
    </div>
  )
})
export default FlowElement
