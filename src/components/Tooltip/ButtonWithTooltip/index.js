import React from 'react'
import './style.scss'

const ButtonWithTooltip = React.memo(({ children, tooltip }) => {
  //
  return (
    <div className='row-button'>
      {children}
      {tooltip ? <div className='tooltip gfs-body'>{tooltip}</div> : null}
    </div>
  )
})
export default ButtonWithTooltip
