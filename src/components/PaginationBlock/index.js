import React, { useEffect, useRef, useState } from 'react'
import './style.scss'
import '../../theme/assets/styles/global.scss'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const PaginationBlock = React.memo(
  ({ cb, totalPages, currentPage, withQueryParam, children }) => {
    const [value, setValue] = useSearchParamsState({
      name: 'page',
      deserialize: (v) => v || ''
    })

    let arr = []
    // const testarr = [...new Array(totalPages).keys()]
    const testarr = new Array(totalPages).fill(' ').map((_, idx) => {
      return idx
    })
    if (totalPages > 10) {
      arr = testarr.slice(0, 10)
      if (currentPage > 5) {
        arr = testarr.slice(currentPage - 5, currentPage + 5)
      }
      if (currentPage > totalPages - 5) {
        arr = testarr.slice(totalPages - 10, totalPages)
      }
    } else {
      arr = testarr
      if (totalPages === 1) {
        arr = []
      }
    }
    const prev = () => {
      // debugger;
      if (currentPage > 1) {
        const prevVal = currentPage - 1
        // if (withQueryParam) {
        //   setParam({ page: prevVal })
        // }
        if (withQueryParam) {
          setValue(prevVal)
        }
        cb(prevVal)()
      }
    }
    const next = () => {
      // debugger;
      if (currentPage < totalPages) {
        const nextVal = currentPage + 1
        if (withQueryParam) {
          setValue(nextVal)
        }

        cb(nextVal)()
      }
    }

    useEffect(() => {
      if (value) {
        cb(Number(value))()
      }
    }, [value])

    const current = (val) => () => {
      const page = val + 1
      if (withQueryParam) {
        setValue(page)
      }
      cb(page)()
    }

    return (
      <div className='pagination-block'>
        <div className='pagination-block-inner'>
          {currentPage > 1 && (
            <button
              type='button'
              onClick={prev}
              className='pagination-block-item gfs-body'
            >
              «
            </button>
          )}
          {arr.map((item) => (
            <button
              type='button'
              key={item}
              className={`pagination-block-item gfs-body ${
                item + 1 === currentPage ? 'target' : ''
              }`}
              onClick={current(item)}
            >
              {item + 1}
            </button>
          ))}{' '}
          {currentPage < totalPages && (
            <button
              type='button'
              onClick={next}
              className='pagination-block-item gfs-body'
            >
              »
            </button>
          )}
        </div>
        {children}
      </div>
    )
  }
)

export default PaginationBlock
