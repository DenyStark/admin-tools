import React, { useEffect } from 'react'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const useDropdown = (itemList, id, title, startValue, queryName) => {
  const [currentItem, setCurrentItem] = React.useState(
    startValue
      ? itemList.find((item) => item[id] === startValue[id])
      : itemList[0]
  )
  const [filtredList, setFiltredList] = React.useState([])

  const [value] = useSearchParamsState({
    name: queryName,
    deserialize: (v) => v || ''
  })

  React.useEffect(() => {
    if (startValue) {
      setFiltredList(itemList.filter((item) => item[id] !== startValue[id]))
    } else {
      setCurrentItem(itemList[0])
      setFiltredList(itemList.filter((item) => item[id] !== itemList[0][id]))
    }
  }, [itemList, id, startValue])

  useEffect(() => {
    if (!queryName) return
    if (value) {
      const valueItem = itemList?.find((el) => el[id] === value)

      if (valueItem) {
        setCurrentItem(valueItem)
        setFiltredList(itemList.filter((item) => item[id] !== valueItem[id]))
      } else {
        setCurrentItem(itemList[0])
        setFiltredList(
          itemList.filter((item) => item[id] !== itemList[0]?.[id])
        )
      }
    }
  }, [value, itemList])

  const setItem = React.useCallback(
    (par) => () => {
      setCurrentItem(itemList.find((item) => item[id] === par))
      setFiltredList(itemList.filter((item) => item[id] !== par))
    },
    [itemList, id]
  )

  return {
    currentItem,
    itemList: filtredList,
    changeItem: setItem,
    id,
    title
  }
}

export default useDropdown
