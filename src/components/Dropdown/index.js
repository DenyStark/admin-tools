import React, { useEffect } from 'react'
import { ReactComponent as DDIcon } from '../../theme/assets/images/icons/dropdownIcon.svg'
import { ClickOutComponent } from '../../Helpers'
import './style.scss'
import '../../theme/assets/styles/global.scss'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const Dropdown = React.memo(
  ({
    current,
    className,
    itemList,
    changeItem,
    id,
    title,
    customClass,
    name,
    queryName,
    cb,
    transform,
    required
  }) => {
    const [open, setOpen] = React.useState(false)
    const [_, setValue] = useSearchParamsState({
      name: queryName,
      deserialize: (v) => v || ''
    })

    const toggle = () => setOpen((prev) => !prev)
    const close = () => {
      setOpen(false)
    }
    const click = React.useCallback(
      (item) => () => {
        toggle()
        if (queryName) {
          // setParam({ [queryName]: item[id] })
          setValue(item[id])
        }
        changeItem(item[id])
        if (cb) {
          cb(item)
        }
      },
      [cb, changeItem, id]
    )

    return (
      <ClickOutComponent onClickOut={close}>
        <div
          className={`dropdown-menu ${
            open ? 'open' : ''
          } ${customClass} ${className}`}
        >
          <button
            className={`dropdown-menu-button gfs-body elements ${customClass}-button ${
              open ? 'open' : ''
            }`}
            type='button'
            onClick={toggle}
          >
            {transform ? transform(current[title], current) : current[title]}
            <div className='title gfs-small'>
              {required && !name.includes('*') ? name + ' *' : name}
            </div>
            <DDIcon />
          </button>
          {open && (
            <div className={`dropdown-menu-items ${customClass}-items`}>
              {itemList.map((item) => (
                <button
                  className={`dropdown-menu-item elements gfs-body ${customClass}-item`}
                  key={item[id]}
                  type='button'
                  onClick={click(item)}
                >
                  {transform ? transform(item[title], item) : item[title]}
                </button>
              ))}
            </div>
          )}
        </div>
      </ClickOutComponent>
    )
  }
)

export default Dropdown
export { default as useDropdown } from './useDropdown'
