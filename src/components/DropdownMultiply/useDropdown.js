import React from 'react'

const defaultVar = []

const useDropdown = (itemList, id, title, startValue) => {
  const [currentItem, setCurrentItem] = React.useState(defaultVar)
  const toggleAllItems = React.useCallback(() => {
    if (itemList.length === currentItem.length) {
      setCurrentItem(defaultVar)

      return
    }
    setCurrentItem([...itemList])
  }, [itemList, currentItem])
  React.useEffect(() => {
    setCurrentItem([...itemList])
  }, [itemList])

  const removeItem = React.useCallback((include) => {
    setCurrentItem((prev) => {
      return [...prev.slice(0, include), ...prev.slice(include + 1)]
    })
  }, [])

  const addItem = React.useCallback(
    (par) => {
      setCurrentItem((prev) => {
        const findItem = itemList.find((item) => item[id] === par)
        return [...prev, findItem]
      })
    },
    [id, itemList]
  )

  const setItem = React.useCallback(
    (par) => () => {
      const include = currentItem.findIndex((item) => item[id] === par)
      if (!par) {
        toggleAllItems()
        return
      }

      if (include >= 0) {
        removeItem(include)
        return
      }

      addItem(par)
    },
    [currentItem, addItem, id, toggleAllItems, removeItem]
  )

  const setCustom = React.useCallback(
    (arrayForSet) => {
      setCurrentItem(
        arrayForSet.map((item) => {
          return itemList.find((el) => el[id] === item)
        })
      )
    },
    [id, itemList]
  )

  if (!itemList.length) {
    return {
      currentItem: defaultVar,
      itemList: [],
      changeItem: () => {},
      id,
      title,
      toggleAll: () => {}
    }
  }
  return {
    currentItem,
    itemList,
    changeItem: setItem,
    id,
    title,
    setCustom
  }
}

export default useDropdown
