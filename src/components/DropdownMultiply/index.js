import React from 'react'
import { ReactComponent as DDIcon } from '../../theme/assets/images/icons/dropdownIcon.svg'
import { ClickOutComponent, replaceStr } from '../../Helpers'
import './style.scss'
import '../../theme/assets/styles/global.scss'

const buttonName = (arr, title) => {
  const length = arr.length

  if (!length) {
    return 'Not selected'
  }

  const list = arr.map((el) => el[title]).join(', ')
  return `Selected: ${replaceStr(list, 18)}`
}

const Dropdown = React.memo(
  ({
    current,
    itemList,
    changeItem,
    id,
    title,
    customClass,
    name,
    cb,
    transform,
    required,
    transformName
  }) => {
    const [open, setOpen] = React.useState(false)
    const toggle = () => {
      setOpen((prev) => !prev)
      if (cb) {
        cb(current)
      }
    }
    const close = () => {
      setOpen(false)
      if (cb) {
        cb(current)
      }
    }

    const click = React.useCallback(
      (item) => () => {
        if (!item) {
          changeItem(null, item, current, itemList)()
        } else {
          changeItem(item[id], item, current, itemList)()
        }
        //   toggle()
      },
      [cb, changeItem, id]
    )

    return (
      <ClickOutComponent onClickOut={close}>
        <div
          className={`dropdown-menu_multiply ${
            open ? 'open' : ''
          } ${customClass}`}
        >
          <button
            className={`dropdown-menu_multiply-button gfs-body elements ${customClass}-button ${
              open ? 'open' : ''
            }`}
            type='button'
            onClick={toggle}
          >
            {transformName
              ? transformName(current, title)
              : buttonName(current, title)}
            <div className='title gfs-small'>
              {required && !name.includes('*') ? name + ' *' : name}
            </div>
            <DDIcon />
          </button>
          {open && (
            <div
              className={`dropdown-menu_multiply-items ${customClass}-items`}
            >
              {itemList.length ? (
                <button
                  className={`dropdown-menu_multiply-item elements gfs-body ${customClass}-item`}
                  type='button'
                  onClick={click()}
                >
                  {current.length === itemList.length ? (
                    <i className='dropdown-menu_multiply-selected fas fa-check-circle' />
                  ) : null}
                  Select All
                </button>
              ) : null}
              {itemList.map((item) => (
                <button
                  className={`dropdown-menu_multiply-item elements gfs-body ${customClass}-item`}
                  key={item[id]}
                  type='button'
                  onClick={click(item)}
                >
                  {current.find((el) => el[id] === item[id]) ? (
                    <i className='dropdown-menu_multiply-selected fas fa-check-circle' />
                  ) : (
                    ''
                  )}{' '}
                  {transform ? transform(item[title], item) : `${item[title]}`}
                </button>
              ))}
            </div>
          )}
        </div>
      </ClickOutComponent>
    )
  }
)

export default Dropdown
export { default as useDropdownMultiply } from './useDropdown'
