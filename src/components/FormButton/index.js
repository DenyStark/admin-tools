/* eslint-disable react/button-has-type */
import React from 'react'
import { access } from '../../Helpers'
import '../../theme/assets/styles/global.scss'
import './style.scss'
// test

const FormButton = React.memo(
  ({
    onClick,
    type = 'button',
    customClass = '',
    children,
    small,
    color,
    checkAccess,
    accesses,
    disabled,
    ...props
  }) => {
    return (
      <button
        {...props}
        type={type}
        disabled={disabled || (checkAccess && access(accesses, checkAccess))}
        onClick={onClick}
        className={` form-button gfs-bold
          ${color} ${customClass} ${small ? 'small' : ''}`}
      >
        {children}
      </button>
    )
  }
)

export default FormButton
