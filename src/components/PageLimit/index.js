import React, { useCallback, useEffect } from 'react'

import Dropdown, { useDropdown } from '../Dropdown'
import { useSearchParamsState } from '../../Helpers/useQueryParam'
import { useDispatch } from 'react-redux'

const PageLimit = ({ limitPage, pageLimit, cb, hide, withQueryParam }) => {
  const { currentItem, itemList, changeItem, id, title } = useDropdown(
    limitPage,
    'id',
    'id',
    pageLimit
  )
  const dispatch = useDispatch()
  const [value, setValue] = useSearchParamsState({
    name: 'limit',
    deserialize: (v) => v || ''
  })

  React.useEffect(() => {
    if (currentItem) {
      cb(currentItem.id)
    }
  }, [currentItem, cb])

  const handleChange = (e) => {
    if (withQueryParam) {
      setValue(e)
    }
    if (changeItem) {
      changeItem(e)()
    }
  }

  const set = useCallback(
    (id) => {
      cb(id)
      if (changeItem) {
        changeItem(id)?.()
      }
      setValue(id)
      dispatch.commonData.changePageLimit(id)
    },
    [dispatch.commonData, changeItem]
  )

  useEffect(() => {
    if (!withQueryParam) return

    if (
      value &&
      currentItem &&
      Number(value) !== currentItem.id &&
      itemList?.length
    ) {
      const valueItem = itemList.find((el) => el[id] === Number(value))
      if (valueItem) {
        set(valueItem[id])
        return
      } else {
        set(currentItem.id)
        return
      }
    }
    cb(currentItem.id)
  }, [currentItem, itemList])

  return hide ? null : (
    <div className='pagelimit'>
      <Dropdown
        current={currentItem}
        itemList={itemList}
        changeItem={handleChange}
        id={id}
        title={title}
        name='Limit'
      />
    </div>
  )
}
export default PageLimit
