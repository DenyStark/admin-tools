import React, { useEffect } from 'react'
import ButtonWithTooltip from '../Tooltip/ButtonWithTooltip'
// import  from '../../theme/assets/styles/global.scss'
import './style.scss'
import '../FormButton/style.scss'
import { useSearchParamsState } from '../../Helpers/useQueryParam'

const ToolButton = React.memo(
  ({
    tooltip,
    icon,
    click,
    active,
    customClass,
    text,
    disabled,
    queryName,
    index,
    symbol
  }) => {
    const [value, updateValue] = useSearchParamsState({
      name: `${queryName}[${index}]`
    })

    useEffect(() => {
      if (value && click && queryName) {
        if (value && !active) {
          click(value)
        }
      }
      if (!value && active && symbol) {
        updateValue(symbol)
      }
    }, [value, active, queryName, symbol])

    const handleClick = () => {
      if (symbol && queryName) {
        if (value && active) {
          updateValue('')
        }

        if (!value && !active) {
          updateValue(symbol)
        }
      }
      if (click) {
        click()
      }
    }

    return (
      <ButtonWithTooltip tooltip={tooltip} key={icon}>
        <button
          className={`form-button white  ${disabled ? 'disabled' : ''} ${
            customClass ? `${customClass}` : 'waiting-button'
          } ${active ? 'black' : ''}`}
          type='button'
          onClick={handleClick}
          disabled={disabled}
        >
          {text}
          {icon ? <i className={icon} /> : null}
        </button>
      </ButtonWithTooltip>
    )
  }
)

export default ToolButton
