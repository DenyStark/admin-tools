import React from 'react'
import './style.scss'

const Sections = React.memo(({ sections, currentSection, setSection, t }) => {
  return sections ? (
    <div className='sections'>
      {sections.map((item) => (
        <button
          key={item}
          className={`pages-button section ${
            item === currentSection ? 'active' : ''
          }`}
          type='button'
          onClick={setSection(item)}
        >
          {t(item)}
        </button>
      ))}
    </div>
  ) : null
})
export default Sections
