import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react'

import CsvDownload from '../CsvDownload'
import Rows from './Row'
import './style.scss'
import '../../theme/assets/styles/global.scss'
import { splitStringBySeparator, firstCharUpper } from '../../Helpers'

const TableCSV = React.memo(({ downloadCSV, csvName }) => {
  return (
    <Fragment>
      {downloadCSV && (
        <Fragment>
          {Array.isArray(downloadCSV) ? (
            <div className='csvs-wrapper'>
              {downloadCSV.map((item) => (
                <CsvDownload
                  key={item.csvName}
                  cb={item.func}
                  filename={item.csvName}
                  text={item.text}
                  icon={item.icon || 'fas fa-download'}
                />
              ))}
            </div>
          ) : (
            <CsvDownload
              cb={downloadCSV}
              filename={csvName}
              icon='fas fa-download'
            />
          )}
        </Fragment>
      )}
    </Fragment>
  )
})

const Table = ({
  csvName,
  tableName,
  className,
  headers,
  rows,
  buttons,
  attention,
  downloadCSV,
  transform,
  keyForRow,
  elementIdForButton,
  checkActive,
  fitTable,
  sort,
  sortAction,
  deepHeaders,
  tableButtonClassName,
  headerTextTransform = (item) => {
    return firstCharUpper(splitStringBySeparator(item).toLowerCase())
  }
}) => {
  if (!headers) {
    return null
  }

  const [widths, setWidths] = useState([])
  const [nodeElements, setNodeElements] = useState([])
  const [headersWidths, setHeaders] = useState(null)
  const refHeadersWidths = useRef([])
  const [buttonEmptyWidth, setButtonEmptyWidth] = useState(0)
  const buttonEmptyRef = useRef(null)

  const handleSetWidths = useCallback((newWidths) => {
    setNodeElements((prev) => [...prev, newWidths])
  }, [])

  useEffect(() => {
    if (!fitTable) return
    const mutate = []
    nodeElements.forEach((el, index) => {
      const values = Object.values(el)

      values?.forEach((el, i) => {
        const elWidth = el?.offsetWidth
        if (mutate[i] && mutate[i] < elWidth) {
          mutate[i] = elWidth
        }
        if (!mutate[i]) {
          mutate[i] = elWidth
        }
      })
    })

    setWidths(mutate)
  }, [nodeElements, fitTable, rows])

  useEffect(() => {
    if (!fitTable) return

    if (!headersWidths && refHeadersWidths.current?.length) {
      setHeaders(refHeadersWidths.current)
    }
  }, [refHeadersWidths, fitTable, headersWidths])

  const fitTableStyleHeader = fitTable
    ? {
        justifyContent: 'left',
        width: 'fit-content',
        marginRight: '30px',
        flexGrow: 0
      }
    : { justifyContent: 'center' }

  const centerHeaderStyle = useMemo(
    () =>
      fitTable || deepHeaders
        ? {}
        : {
            position: 'absolute',
            left: '50%',
            top: '50%',
            transform: 'translate(-50%, -50%)'
          },
    [fitTable, deepHeaders]
  )

  const secondFloorHeaders = useMemo(
    () =>
      headers
        .map((column) => {
          if (column && typeof column === 'object') {
            return column
          }
        })
        .filter((el) => el),
    [headers]
  )

  const flatted = secondFloorHeaders.flat()

  const renderSecondFloor = useMemo(
    () =>
      flatted.map((el, i) => {
        let rightBorder = false
        let leftBorder = false
        const value = el && el.split('.')
        const currentValue = value[value.length - 1]

        if (deepHeaders) {
          const nextValue = flatted[i + 1] && flatted[i + 1].split('.')
          const prevValue = flatted[i - 1] && flatted[i - 1].split('.')

          if (
            (value && prevValue && nextValue && value[0] !== nextValue[0]) ||
            nextValue?.length > 1
          ) {
            rightBorder = true
          }

          if (!prevValue || (prevValue && value[0] !== prevValue[0])) {
            leftBorder = true
          }
        }

        if (!value) return
        const formatted = headerTextTransform
          ? headerTextTransform(currentValue)
          : firstCharUpper(splitStringBySeparator(currentValue).toLowerCase())
        return (
          <th
            className={`table-headers-item ${currentValue}-header ${
              leftBorder ? 'left-border' : ''
            } ${rightBorder ? 'right-border' : ''}`}
            key={el + i}
          >
            {formatted}
          </th>
        )
      }),
    [secondFloorHeaders]
  )

  const deep = headers.reduce((acc, el) => {
    const splitted =
      el && typeof el === 'object' && Array.isArray(el)
        ? Math.max(
            ...el.map((el) => {
              return el?.split('.').length
            })
          )
        : 0
    acc = splitted > acc ? splitted : acc
    return acc
  }, 1)

  useEffect(() => {
    if (buttonEmptyRef?.current) {
      setButtonEmptyWidth(buttonEmptyRef?.current?.offsetWidth)
    }
  }, [buttonEmptyRef?.current])

  let k = null

  return (
    <table
      cellSpacing={0}
      cellPadding={0}
      className={`table ${tableName} ${className} ${
        deepHeaders ? 'table-fixed' : ''
      } ${downloadCSV ? 'csv-spacing' : ''}`}
    >
      <thead>
        <tr className={`table-CSV ${downloadCSV ? 'withCSV' : ''}`}>
          <td>
            <TableCSV downloadCSV={downloadCSV} csvName={csvName} />
          </td>
        </tr>
        <tr
          className={`table-headers gfs-bold ${tableName}-header`}
          style={fitTable ? { justifyContent: 'flex-start' } : {}}
        >
          {(headers || []).map((item, i) => {
            const hasSubColumn = typeof item === 'object' && Array.isArray(item)
            const prevHeader =
              i && headers[i - 1] && Array.isArray(headers[i - 1])
            const splittedItem = hasSubColumn && item?.[0]?.split('.')
            const formattedItem = hasSubColumn ? splittedItem?.[0] : item
            const hasSort = sort && sort?.sortFields?.includes(formattedItem)
            const activeSort = sort && sort?.activeSort === formattedItem
            const hasSortType = sort && sort?.sortType
            const sortType =
              hasSortType && sort?.sortType?.toLowerCase() === 'desc'
            const minWidth =
              fitTable && widths?.[i]
                ? {
                    width: `${
                      (headersWidths?.[i] >= widths?.[i]
                        ? headersWidths?.[i]
                        : widths?.[i]) + (activeSort ? 18 : 0)
                    }px`
                  }
                : {}

            return (
              <th
                key={formattedItem}
                rowSpan={hasSubColumn ? 1 : deep}
                colSpan={hasSubColumn ? item.length : 1}
                className={`${
                  hasSubColumn || prevHeader ? 'left-border bottom-border' : ''
                } ${hasSubColumn}`}
                style={{ ...minWidth }}
              >
                <div
                  onClick={() =>
                    hasSort && sortAction && sortAction(formattedItem)
                  }
                  className={`table-headers-item ${formattedItem}-header`}
                  style={{
                    ...fitTableStyleHeader,
                    ...minWidth,
                    display: 'flex',
                    textAlign: fitTable ? 'left' : 'center',
                    cursor: hasSort ? 'pointer' : 'default',
                    whiteSpace: 'nowrap',
                    ...centerHeaderStyle
                  }}
                  ref={(ref) => {
                    if (fitTable && ref && ref?.offsetWidth) {
                      refHeadersWidths.current.push(ref?.offsetWidth)
                    }
                  }}
                >
                  {headerTextTransform(formattedItem)}
                  {activeSort && (
                    <i
                      style={{
                        marginLeft: '10px',
                        display: 'flex',
                        alignItems: 'center'
                      }}
                      className={
                        hasSortType
                          ? sortType
                            ? 'fas fa-sort-up'
                            : 'fas fa-sort-down'
                          : 'fas fa-sort'
                      }
                    ></i>
                  )}
                </div>
              </th>
            )
          })}
          {buttons && !fitTable ? (
            <th
              rowSpan={1}
              colSpan={deepHeaders ? 1 : 2}
              className={`table-headers-buttons ${tableName}-header-buttons`}
              style={{
                width: buttonEmptyWidth ? `${buttonEmptyWidth}px` : 'auto'
              }}
            />
          ) : null}
        </tr>
        {deepHeaders && (
          <tr className={`table-headers gfs-bold ${tableName}-header`}>
            {renderSecondFloor}
          </tr>
        )}
      </thead>
      <tbody className={`table-body ${tableName}-body`}>
        {rows?.map((item, i) => (
          <Rows
            deepHeaders={deepHeaders}
            buttonEmptyRef={buttonEmptyRef}
            fitTable={fitTable}
            headersWidths={headersWidths}
            widths={widths}
            handleSetWidths={handleSetWidths}
            key={keyForRow ? keyForRow(item, i) : i}
            keyForBtn={elementIdForButton || 0}
            headers={headers?.flat()}
            row={item}
            buttons={buttons}
            attention={attention}
            transform={transform}
            checkActive={checkActive}
            tableButtonClassName={tableButtonClassName}
          />
        ))}
      </tbody>
    </table>
  )
}

export default Table
