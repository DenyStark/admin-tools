import React, { useEffect, useRef } from 'react'
import ToolButton from '../../ToolButton'
import '../style.scss'
import '../../../theme/assets/styles/global.scss'
import { get } from 'lodash'

const attencionToColor = (attention, row) => {
  if (typeof attention !== 'function') {
    return ''
  }
  switch (attention(row)) {
    case 1:
      return 'attention'
    case 2:
      return 'sent'
    case 3:
      return 'delivered'
    default:
      return ''
  }
}

const EmptyButton = () => {
  return <div style={{ width: '34px', height: '34px' }}></div>
}

const RowButton = (props) => {
  return <ToolButton {...props} />
}
const Row = React.memo(
  ({
    headers,
    row,
    buttons,
    attention,
    transform,
    keyForBtn,
    checkActive,
    widths,
    headersWidths,
    handleSetWidths,
    fitTable,
    buttonEmptyRef,
    deepHeaders,
    tableButtonClassName
  }) => {
    const rowRef = useRef({})
    useEffect(() => {
      if (!fitTable) return
      const length = Object.keys(rowRef.current)?.length
      if (length === headers.length && handleSetWidths) {
        handleSetWidths(rowRef.current)
      }
    }, [rowRef, row, fitTable])

    const tableRowButtonStyleName = tableButtonClassName ? tableButtonClassName : 'table-row-buttons';

    const fitTableStyleRow = fitTable
      ? {
          justifyContent: 'left',
          width: 'fit-content',
          marginRight: '30px',
          flexGrow: 0
        }
      : {}

    let prevKey = null
    return (
      <tr
        className={`table-body-row gfs-body ${
          attention ? attencionToColor(attention, row) : ''
        }`}
        style={fitTable ? { justifyContent: 'flex-start' } : {}}
      >
        {headers.map((item, i) => {
          const value = item && item.split('.')
          const prevHeaderHasSub =
            i && headers[i - 1] && headers[i - 1].split('.')?.length > 1
          const withoutSubHeaderButFirstAfterIt =
            prevHeaderHasSub && value?.length === 1

          const hasSubValue = Array.isArray(value) && value?.length > 1

          let leftBorder = false

          if (deepHeaders) {
            if (hasSubValue && prevKey && prevKey !== value[0]) {
              leftBorder = true
              prevKey = value[0]
            }

            if (
              (hasSubValue && !prevKey) ||
              withoutSubHeaderButFirstAfterIt ||
              prevHeaderHasSub
            ) {
              leftBorder = true
            }

            if (!prevKey) {
              prevKey = value ? value[0] : null
            }
          }

          const minWidth =
            fitTable && widths?.[i] && headersWidths?.[i]
              ? {
                  width: `${
                    headersWidths?.[i] >= widths?.[i]
                      ? headersWidths?.[i]
                      : widths?.[i]
                  }px`
                }
              : {}

          return (
            <td
              style={{ ...minWidth }}
              key={i}
              className={`${leftBorder ? 'left-border' : ''}`}
            >
              <div
                className={`table-row-item ${item}-row`}
                key={item}
                style={{
                  ...fitTableStyleRow,
                  ...minWidth,
                  textAlign: fitTable ? 'left' : 'center'
                }}
                ref={(ref) => {
                  if (ref && fitTable) {
                    rowRef.current[ref?.className] = ref
                  }
                }}
              >
                {transform
                  ? transform(get(row, item), item, row)
                  : get(row, item)?.toString() || 'none'}
              </div>
            </td>
          )
        })}
        {buttons && (
          <td className={tableRowButtonStyleName} ref={buttonEmptyRef}>
            {buttons.map((item, i) => {
              if (item?.hideButton && item?.hideButton(row)) {
                return <EmptyButton key={i} />
              }
              if (
                checkActive &&
                item?.inactiveButton &&
                item.inactiveButton(row)
              ) {
                return (
                  <RowButton
                    key={item.icon || i}
                    icon={item.icon}
                    tooltip={item.tooltip}
                    disabled
                    click={item.cb(row[headers[keyForBtn]], row)}
                  />
                )
              }
              return (
                <RowButton
                  key={item.icon || i}
                  icon={item.icon}
                  tooltip={item.tooltip}
                  click={item.cb(row[headers[keyForBtn]], row)}
                />
              )
            })}
          </td>
        )}
      </tr>
    )
  }
)
export default Row
