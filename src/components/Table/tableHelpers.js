import { useHistory } from 'react-router-dom'

export const useSearch = (link) => {
  const history = useHistory()
  //  const dispatch = useDispatch();
  const UserClick = (value) => async () => {
    await history.push(`${link}${value ? `/${value}` : ''}`)
    //  dispatch.commonData.changeSearchParams(value);
  }

  return UserClick
}
