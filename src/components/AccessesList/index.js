import React from 'react'
import './style.scss'

export const useAccesses = (accesses) => {
  const [accessData, setAccessData] = React.useState(accesses)
  const addItem = (item) => () => {
    setAccessData((prev) => {
      if (prev.includes(item) || prev.includes(`${item}+`)) {
        return prev.filter((el) => el !== item && el !== `${item}+`)
      }
      return [...prev, item]
    })
  }
  const plus = (item) => () => {
    setAccessData((prev) => {
      if (prev.includes(`${item}+`)) {
        return prev.map((el) => {
          if (el === `${item}+`) {
            return item
          }
          return el
        })
      }
      if (prev.includes(item)) {
        return [...prev.filter((el) => el !== item), `${item}+`]
      }

      return [...prev, `${item}+`]
    })
  }
  return { addItem, plus, accessData }
}

const AccessElement = React.memo(
  ({ item, addItem, plus, data = [], fieldName }) => {
    const [el, setEl] = React.useState('')

    React.useEffect(() => {
      setEl(data.find((it) => it.includes(item.access)))
    }, [data, item.access])

    return (
      <div
        className={`access-list-element ${item.plus ? 'big' : ''} ${
          el && el.includes(item.access) ? 'active' : ''
        }`}
      >
        <button
          className={`gfs-tiny access-list-element-item `}
          type='button'
          onClick={addItem(item.access)}
        >
          {fieldName ? item[fieldName] : item.access}
        </button>
        {item.plus ? (
          <button
            type='button'
            className={`gfs-tiny access-list-element-plus ${
              el && el.includes(`${item.access}+`) ? 'active' : ''
            }`}
            onClick={plus(item.access)}
          >
            +
          </button>
        ) : null}
      </div>
    )
  }
)

const AccessesList = React.memo(
  ({ accesses, addItem, data, plus, fieldName }) => {
    return (
      <div className='access-list'>
        {accesses.map((item) => (
          <AccessElement
            item={item}
            plus={plus}
            addItem={addItem}
            key={item.access}
            data={data}
            fieldName={fieldName}
          />
        ))}
      </div>
    )
  }
)

export default AccessesList
