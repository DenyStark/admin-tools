import React from 'react'
import Chart from 'chart.js'
import CSVDownloads from '../CsvDownload'
import './style.scss'

const sum = (...values) => {
  return values.reduce((acc, curr) => +acc + +curr)
}

const getPercentage = (currentValue, maxValue) => {
  const percentage = (currentValue * 100) / maxValue
  return percentage.toFixed(2)
}

export const colorGraph = (size, set) => {
  const colors = [
    '#000000',
    '#202020',
    '#404040',
    '#A9A9A9',
    '#C5C5C5',
    '#F2F2F2'
  ]
  if (set) {
    return ['#000000', '#A9A9A9']
  }

  switch (size) {
    case 2:
      return [colors[0], colors[5]]
    case 3:
      return [colors[0], colors[3], colors[5]]
    case 4:
      return [colors[0], colors[2], colors[3], colors[5]]
    case 5:
      return [colors[0], colors[2], colors[3], colors[4], colors[5]]
    case 6:
      return colors
    default:
      return []
  }
}

const addChar = (address, { data, labels }, options) => {
  const params = {
    type: 'doughnut',
    data: {
      labels,
      datasets: [
        {
          label: '# of Votes',
          data,
          backgroundColor: colorGraph(data.length)
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      layout: {
        padding: 15
      }
    }
  }

  // eslint-disable-next-line no-new
  return new Chart(
    address,
    typeof options === 'function' ? options(params) : params
  )
}
const arrayTrans = (arr, innerArrayLength) => {
  const length = Math.ceil(arr.length / innerArrayLength)
  console.log(length)
  const wrapperArray = new Array(length).fill([])
  const fillledArray = wrapperArray.map((item, wrapperIdx) => {
    return [
      ...arr.filter((innerItem, innerIdx) => {
        return (
          innerIdx < innerArrayLength * (wrapperIdx + 1) &&
          innerArrayLength * wrapperIdx <= innerIdx
        )
      })
    ]
  })
  return fillledArray
}

const ChartsBuilder = React.memo(
  ({
    data = {},
    header,
    legendTransform,
    valueUnderGrapthTransform,
    csvDownload,
    csvName,
    csvTitle,
    options,
    noLegends,
    height = '180',
    width = '180',
    noPercentage,
    changeTitleForLegend
  }) => {
    const currentChart = React.useRef()
    React.useEffect(() => {
      if (!ref) {
        return
      }
      const ctx = ref.current.getContext('2d')
      if (currentChart.current) {
        currentChart.current.destroy()
      }

      currentChart.current = addChar(ctx, data, options)
    }, [data])
    const ref = React.useRef()
    const legends = arrayTrans(data.data, 4)
    const labels = arrayTrans(data.labels, 4)
    return (
      <div className='chartDonat-wrapper'>
        {csvDownload && (
          <CSVDownloads
            cb={csvDownload}
            filename={csvName}
            text={csvTitle}
            icon='fas fa-download'
          />
        )}

        <div className='charts chartDonat'>
          <div className='charts-info'>
            <div
              className='charts-builder'
              style={{
                position: 'relative',
                height: `${height}px`,
                width: `${width}px`
              }}
            >
              <canvas ref={ref} width={`${width}`} height={`${height}`} />
            </div>
            <div className='charts-title'>
              <div className='value gfs-h1'>
                {valueUnderGrapthTransform
                  ? valueUnderGrapthTransform(data)
                  : data.data.length > 0 &&
                    data.data?.reduce((acc, curr) => acc + curr).toFixed(2)}
              </div>
              <div className='header gfs-small'>{header}</div>
            </div>
          </div>
          {noLegends ? null : (
            <div className='charts-legend'>
              {legends &&
                legends.map((wrappedItem, wrappedIn) => (
                  <div key={wrappedIn}>
                    {wrappedItem.map((item, idx) => (
                      <div className='legends-item' key={`${item}-${idx}`}>
                        <div className='legends-item-data gfs-bold'>
                          {legendTransform
                            ? legendTransform(
                                item,
                                legends,
                                labels,
                                labels[wrappedIn][idx],
                                data
                              )
                            : item.toFixed(2)}
                          {noPercentage
                            ? null
                            : `(${getPercentage(item, sum(...data.data))}%)`}
                        </div>
                        <div className='legends-item-title gfs-small'>
                          {changeTitleForLegend
                            ? changeTitleForLegend(
                                item,
                                legends,
                                labels,
                                labels[wrappedIn][idx],
                                data
                              )
                            : labels[wrappedIn][idx]}
                        </div>
                      </div>
                    ))}
                  </div>
                ))}
            </div>
          )}
        </div>
      </div>
    )
  }
)
export default ChartsBuilder
