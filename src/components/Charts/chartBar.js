import React from 'react'
import Chart from 'chart.js'
import './style.scss'
import CSVDownloads from '../CsvDownload'
import { colorGraph } from './chartDonat'
import { firstCharUpper } from '../../Helpers'

const getRange = (max, min, percent) => {
  return (max - min) / percent
}

const addColor = (array, stack, subData, innerFunc) => {
  // console.log(array, stack);
  const datasets = array
    .map((item, idx) => {
      const val = {
        ...item,
        backgroundColor:
          subData && innerFunc
            ? innerFunc(array[subData])
            : colorGraph(array.length, true)[idx]
      }
      val.type = 'bar'
      if (stack) {
        val.stack = 'Stack 0'
      }
      return val
    })
    .filter((_, idx) => idx !== subData)
  //  console.log(datasets);
  return datasets
}

const addLabels = (data, types) => {
  return data.map((item, idx) => {
    const value = { ...item }
    if (types && types[idx]) {
      value.label = firstCharUpper(types[idx])
    }
    return value
  })
}
const addZeroLine = (arr, value) => {
  return [
    ...arr,
    {
      type: 'line',
      data: arr[0].data.map(() => value),
      label: null
    }
  ]
}

const addChar = (
  address,
  { datasets, labels },
  graphMax,
  graphMin,
  stack,
  types,
  withLine,
  subData,
  innerFunc,
  addOptions,
  withoutTickSizes
) => {
  const minValue =
    graphMin - getRange(graphMax, graphMin, 10) > 0
      ? graphMin - getRange(graphMax, graphMin, 10)
      : graphMin - 1

  const maxValue = graphMax + getRange(graphMax, graphMin, 10) || 1
  const stepSize = graphMax + getRange(graphMax, graphMin, 10) || 1

  const tickSizes = withoutTickSizes
    ? {}
    : {
        min: minValue,
        max: maxValue,
        stepSize: stepSize
      }

  const params = {
    type: 'bar',
    data: {
      labels,
      datasets:
        withLine && minValue !== 0
          ? addZeroLine(
              addLabels(addColor(datasets, stack, subData, innerFunc), types),
              minValue
            )
          : addLabels(addColor(datasets, stack, subData, innerFunc), types),
      scales: {
        y: {
          //  beginAtZero: true,
        }
      }
    },
    options: {
      scaleBeginAtZero: false,
      legend: {
        display: false
      },
      layout: {},
      scales: {
        yAxes: [
          {
            display: true,
            gridLines: {
              lineWidth: 1,
              zeroLineColor: '#C5C5C5',
              zeroLineWidth: 1,
              display: true,
              drawTicks: false,
              color: ['transparent', '#C5C5C5']
            },
            ticks: {
              ...tickSizes,
              // min: minValue,
              // max: maxValue,
              // stepSize: stepSize,
              padding: 5,
              display: false
            }
          }
        ],
        xAxes: [
          {
            display: true,
            gridLines: {
              lineWidth: 0,
              zeroLineColor: '#C5C5C5',
              zeroLineWidth: 1,
              display: true,
              drawTicks: false
            },
            ticks: {
              padding: 5,
              fontSize: 9
            }
          }
        ]
      },
      elements: {
        point: {
          radius: 0
        }
      }
    }
  }

  // eslint-disable-next-line no-new
  return new Chart(address, addOptions ? addOptions(params) : params)
}

const ChartsBar = React.memo(
  ({
    data = {},
    header,
    graphMax = 0,
    graphMin = 0,
    csvDownload,
    csvName,
    stack,
    types,
    withLine,
    csvTitle,
    subData,
    innerFunc,
    height = '200',
    width = '540',
    addOptions,
    minValue,
    displayMax,
    withoutTickSizes
  }) => {
    const currentChar = React.useRef()
    React.useEffect(() => {
      if (!ref) {
        return
      }
      const ctx = ref.current.getContext('2d')

      if (currentChar.current) {
        currentChar.current.destroy()
      }

      currentChar.current = addChar(
        ctx,
        data,
        graphMax,
        graphMin,
        stack,
        types,
        withLine,
        subData,
        innerFunc,
        addOptions,
        withoutTickSizes
      )
    }, [
      data,
      graphMax,
      graphMin,
      stack,
      types,
      withLine,
      subData,
      innerFunc,
      withoutTickSizes
    ])

    const ref = React.useRef()
    // const { datasets: values } = data;

    return (
      <div className='charts chartBar'>
        <div className='charts-info'>
          <div className='charts-title'>
            <div className='title gfs-h2'>{header}</div>
          </div>
          {csvDownload && (
            <React.Fragment>
              {Array.isArray(csvName) ? (
                <div className='charts-csv'>
                  {csvName.map((item, idx) => (
                    <CSVDownloads
                      key={item}
                      cb={csvDownload[idx]}
                      filename={item}
                      icon='fas fa-download'
                      text={csvTitle && csvTitle[idx]}
                    />
                  ))}
                </div>
              ) : (
                <CSVDownloads
                  cb={csvDownload}
                  filename={csvName}
                  text={csvTitle}
                  icon='fas fa-download'
                />
              )}
            </React.Fragment>
          )}
          <div
            className='charts-builder'
            style={{
              position: 'relative',
              height: `${height}px`,
              width: `${width}px`
            }}
          >
            {!withoutTickSizes && (
              <div className='max-values gfs-tiny'>
                {displayMax ? displayMax(graphMax) : graphMax}
              </div>
            )}

            {(graphMin !== 0 || minValue) && !withoutTickSizes && (
              <div className='min-values gfs-tiny'>{graphMin || minValue}</div>
            )}
            <canvas ref={ref} width={`${width}`} height={`${height}`} />
          </div>
        </div>
      </div>
    )
  }
)
export default ChartsBar
