import React from 'react'
import './style.scss'
export const mounthRange = 12
export const initialYear = 2021

export const getLabel = (val) => {
  const theBigDay = new Date()
  const yearCount = Math.floor(+val / mounthRange)
  const mounthCount = val % mounthRange
  theBigDay.setMonth(mounthCount)
  return theBigDay.setFullYear(initialYear + yearCount)
  // return theBigDay;
}

const Label = React.memo(
  ({ button, item, list, click, children, required }) => {
    if (button) {
      return (
        <button
          type='button'
          className={`label-element gfs-tiny ${
            list.includes(item) ? 'black' : 'white'
          }`}
          onClick={click(item)}
        >
          <label>
            <input
              type='checkbox'
              className='label-element-checked'
              required={required}
            />
            {children}
          </label>
        </button>
      )
    }
    return <div className='label-element black gfs-tiny '>{children}</div>
  }
)
export default Label
