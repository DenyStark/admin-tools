import React from 'react'
import { useHistory, useLocation } from 'react-router-dom'

export const useURLChanger = (query, val) => {
  const history = useHistory()
  React.useEffect(() => {
    if (query && val) {
      history.replace(query)
    }
  }, [history, query, val])
}

export const useDowloadInitial = (setters, values) => {
  // const items = values.get('items');
  const loaded = React.useRef(false)
  React.useEffect(() => {
    if (loaded.current) return

    //  setters(+items)();
    Object.keys(setters).forEach((item) => {
      console.log(values.get(item))
      if (values.get(item) === 'all') {
        return
      }
      setters[item](values.get(item))()
    })
    loaded.current = true
  }, [setters, values])
  return loaded.current
}

export const useTransformQuery = (query, data, noQuery) => {
  const { search, pathname } = useLocation()
  //  const query = new URLSearchParams(search);
  // console.log(data);
  // React.useEffect

  const newQuery = React.useMemo(() => {
    Object.keys(data).forEach((item) => {
      if (typeof data[item] === 'object' && !Array.isArray(data[item]))
        return query.set(item, data[item].id)
      if (typeof data[item] === 'object' && Array.isArray(data[item])) {
        if (typeof data[item][0] === 'object') {
          return query.set(item, data[item].map((el) => el.id).join(','))
        }
        return query.set(item, data[item].join(','))
      }
      if (data[item]) {
        return query.set(item, data[item])
      }
      query.delete(item)
    })
    return `${pathname}?${query.toString()}`
  }, [data, pathname, query])
  if (noQuery) {
    return ''
  }
  //  console.log(query, newQuery, data);
  return newQuery
}
