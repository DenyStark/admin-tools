import React from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useRouteMatch, useHistory } from 'react-router-dom'
// add elementId

// function useQuery() {
//   const { search } = useLocation();

//   return React.useMemo(() => new URLSearchParams(search), [search]);
// }
export const usePreloadData = (getData) => {
  const dispatch = useDispatch()
  const [status, setStatus] = React.useState(false)

  React.useLayoutEffect(() => {
    if (status) return
    const get = async () => {
      //   await dispatch.tickets.getEvents();
      const arr = getData.map((item) => {
        const [a, b] = item.split('/')
        return dispatch[a][b]()
      })
      // .forEach(v => v());

      await Promise.allSettled(arr)
      setStatus(true)
    }
    get()
  }, [dispatch, dispatch.tickets, getData, status])
  return status
}

const useGetQuery = (transform) => {
  const { search } = useLocation()
  const query = React.useMemo(() => new URLSearchParams(search), [search])
  const ref = React.useRef(
    transform
      ? transform({ uri: query, transformData: {} })
      : { uri: query, transformData: {} }
  )
  const arr = []
  // eslint-disable-next-line no-restricted-syntax
  for (const [p, q] of query) {
    arr.push([p, q])
  }

  // return arr.reduce((acc, curr) => {
  //   return { ...acc, [curr[0]]: transform ? transform(curr[0], curr[1]) : transformValue(curr[0], curr[1]) };
  // }, {});
  return ref.current
}

export const useWrapperLink = (link) => {
  const history = useHistory()
  const refLink = React.useRef()

  const setInner = () => {
    history.replace(`/${link}/${refLink.current}`)
  }
  const setOuter = () => {
    history.replace(`/${link}`)
  }

  const setLink = (l) => {
    refLink.current = l
  }

  return {
    setLink,
    setInner,
    setOuter
  }
}

const PageWrapper = (Child, getData = [], transformValueFromQuery) => {
  const InnerComponent = ({ params, props }) => {
    //  const { elementId, ...params } = useParams();
    const location = useLocation()
    const prop = useRouteMatch()
    const el = React.useMemo(() => {
      const elementId = location.pathname.split(`${prop.path}/`)[1]

      // const searchParams = new URLSearchParams(location.search)

      return {
        elementId
        //   searchParams
      }
    }, [location, prop.path])
    const [info, setInfo] = React.useState(!!el.elementId)
    return (
      <Child
        {...props}
        openInnerPage={info}
        setInnerInfo={setInfo}
        elementId={el.elementId || ''}
        params={params}
        //  searchParams={el.searchParams || null}
      />
    )
  }

  const WrapperComponent = (props) => {
    const params = useGetQuery(transformValueFromQuery)
    console.log(params)
    const loaded = usePreloadData(getData)

    if (!loaded) return null
    return <InnerComponent params={params} props={props} />
  }
  return WrapperComponent
}
export default PageWrapper
